﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildCavern : MonoBehaviour
{
    public GameObject barracs;
    public GameObject sleepingQuarters;
    public GameObject stockPile;

    public GameObject bBaracs;
    public GameObject bSleep;
    public GameObject bStock;

    public AudioSource buildSound;



    private void Awake()
    {
        barracs.SetActive(false);
        sleepingQuarters.SetActive(false);
        stockPile.SetActive(false);

        bBaracs = GameObject.FindGameObjectWithTag("BuildBarracs");
        bSleep = GameObject.FindGameObjectWithTag("BuildSleep");
        bStock = GameObject.FindGameObjectWithTag("BuildStock");

        bBaracs.GetComponent<Button>().interactable = false;
        bSleep.GetComponent<Button>().interactable = false;
        bStock.GetComponent<Button>().interactable = false;
    }
    private void Update()
    {
        if (MainController.getBasicFood() >= 5)
        {
            bStock.GetComponent<Button>().interactable = true;
        }
        else
        {
            bStock.GetComponent<Button>().interactable = false;
        }
        if (MainController.getBasicFood() >= 15)
        {
            bSleep.GetComponent<Button>().interactable = true;
        }
        else
        {
            bSleep.GetComponent<Button>().interactable = false;
        }
        if (MainController.getBasicFood() >= 20) 
        {
            bBaracs.GetComponent<Button>().interactable = true;            
        }
        else
        {
            bBaracs.GetComponent<Button>().interactable = false;
        }
        Debug.Log("Basic food: " + MainController.getBasicFood());
    }

    public void BuildStockPile()
    {
        buildSound.Play();
        stockPile.SetActive(true);
        Glob.STOCKPILE = true;
        MainController.maxBasicFood = 20;
        MainController.setBasicFood(- 5);
        bStock.SetActive(false);

    }
    public void BuildBarracs()
    {
        buildSound.Play();
        barracs.SetActive(true);
        Glob.BARRACS = true;
        MainController.setBasicFood(- 20);
        bBaracs.SetActive(false);

    }
    public void BuildSleepingQuarters()
    {
        buildSound.Play();
        sleepingQuarters.SetActive(true);
        Glob.SLEEPINGQ = true;
        MainController.maxAnts += 3;
        MainController.setBasicFood(- 15);
        bSleep.SetActive(false);
    }
}
