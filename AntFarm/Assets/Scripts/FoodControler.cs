﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FoodControler : MonoBehaviour
{
    
    public static void GatherFood(GameObject foodSource, int ammount, GameObject gatherer)
    {
        if (foodSource.GetComponent<Food>().basicFood == 0)
        {
            int foodTaken = 0;
            int premiumFood = foodSource.GetComponent<Food>().premiumFood;
            if (premiumFood - ammount < 0)
            {
                foodTaken = premiumFood;
                premiumFood = 0;
            }
            else
            {
                premiumFood = premiumFood - ammount;
                foodTaken = ammount;
            }
            gatherer.GetComponent<Ant>().premiumFood = ammount;
            //MainController.setPremiumFood(ammount);
        }
        else
        {
            foodSource.GetComponent<Food>().basicFood = foodSource.GetComponent<Food>().basicFood - ammount;
            gatherer.GetComponent<Ant>().basicFood = ammount;
            //MainController.setBasicFood(ammount);
        }        
        KillIfEmpty(foodSource);
    }

    public static void KillIfEmpty(GameObject foodSource)
    {
        if (foodSource.GetComponent<Food>().premiumFood < 1 && foodSource.GetComponent<Food>().basicFood < 1)
        {
            Destroy(foodSource);
        }
    }

    public static void DeployFood(GameObject gatherer, GameObject storage)
    {
        int food = gatherer.gameObject.GetComponent<Ant>().premiumFood + gatherer.gameObject.GetComponent<Ant>().basicFood;
        if (gatherer.GetComponent<Ant>().premiumFood > 0)
        {
            int deployedFood = DeploySomeAmmount(gatherer, storage);
            int foodToLeve = food - deployedFood;
            MainController.setPremiumFood(foodToLeve);
            if (deployedFood < food)
            {
                gatherer.gameObject.GetComponent<Ant>().premiumFood -= food - deployedFood;
                gatherer.gameObject.GetComponent<Ant>().SetState(Glob.STATE_RETURNINGWITHFOOD);
            } else if (deployedFood >= food)
            {
                gatherer.gameObject.GetComponent<Ant>().premiumFood = 0;
                gatherer.gameObject.GetComponent<Ant>().SetState(Glob.STATE_SEARCHINGFORFOOD);
            }
        }
        else if (gatherer.GetComponent<Ant>().basicFood > 0)
        {
            int deployedFood = DeploySomeAmmount(gatherer, storage);
            int foodToLeve = food - deployedFood;
            MainController.setBasicFood(food - deployedFood);
            if (deployedFood < food)
            {
                gatherer.gameObject.GetComponent<Ant>().basicFood -= food - deployedFood;
                gatherer.gameObject.GetComponent<Ant>().SetState(Glob.STATE_RETURNINGWITHFOOD);
            }
            else if (deployedFood >= food)
            {
                gatherer.gameObject.GetComponent<Ant>().basicFood = 0;
                gatherer.gameObject.GetComponent<Ant>().SetState(Glob.STATE_SEARCHINGFORFOOD);
            }
        }
        // PRZETESTUJ TO !!
    }

    private static int DeploySomeAmmount(GameObject gatherer, GameObject storage)
    {
        int currentStock = storage.GetComponent<FoodStorage>().GetCurrentStock();
        if (currentStock < FoodStorage.GetMaxStorage())
        {
            int food = gatherer.gameObject.GetComponent<Ant>().basicFood + 
                gatherer.gameObject.GetComponent<Ant>().premiumFood;
            if (food < (FoodStorage.GetMaxStorage() - currentStock))
            {
                currentStock += food;
                return 0;
            }
            else
            {
                int spaceLeft = FoodStorage.GetMaxStorage() - currentStock;
                food -= spaceLeft;
                return food;
            }
        }
        return 0;
    }
    
}
