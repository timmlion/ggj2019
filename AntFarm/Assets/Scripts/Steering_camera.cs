﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Steering_camera : MonoBehaviour
{

    public int speed = 1;
    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D) || (Input.mousePosition.x > Screen.width - 100 && Input.mousePosition.x < Screen.width))
        {
            if (transform.position.x < 14)
            {
                transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
            }
            
        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || (Input.mousePosition.x < 100 && Input.mousePosition.x > 0))
        {
            if (transform.position.x > -14)
            {
                transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
            }
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S) || (Input.mousePosition.y < 100 && Input.mousePosition.y > 0))
        {
            if (transform.position.y > -15.5)
            {
                transform.Translate(new Vector3(0, -speed * Time.deltaTime, 0));
            }
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) || (Input.mousePosition.y > Screen.height - 100 && Input.mousePosition.y < Screen.height))
        {
            if (transform.position.y < 15.5)
            {
                transform.Translate(new Vector3(0, speed * Time.deltaTime, 0));
            }
        }
    }
}
