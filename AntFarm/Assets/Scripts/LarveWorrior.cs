﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class LarveWorrior : MonoBehaviour
{
    public float targetTime = 10.0f;
    public GameObject[] spawnPoints;
    public GameObject worrior;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        targetTime -= Time.deltaTime;

        if (targetTime <= 0.0f)
        {
            timerEnded();
        }
    }

    void timerEnded()
    {
        Random.Range(1, spawnPoints.Length + 1);

        Instantiate(worrior, spawnPoints[Random.Range(1, spawnPoints.Length + 1)].transform.position, Quaternion.identity);
        Destroy(gameObject);

    }
}
