﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainController : MonoBehaviour
{
    public static int maxBasicFood = 5;
    public static int maxPremiumFood = 0;
    public static int maxAnts = 3;
    public static int workers { get; set; }
    public static int worriors { get; set; }



    private static int _basicFood;    
    private static int _premiumFood;
    public static void setBasicFood(int ammount)
    {
        _basicFood += ammount;
        if (_basicFood > maxBasicFood)
        {
            _basicFood = maxBasicFood;
            Debug.Log("Przekroczono maksymalna ilość BasicFood");
        }
    }

    internal static void SubtractFromBasicFood(int v)
    {
        _basicFood -= v;
    }

    public static int getBasicFood()
    {
        return _basicFood;
    }
    public static void setPremiumFood(int ammount)
    {
        _premiumFood += ammount;
        if (_premiumFood > maxPremiumFood)
        {
            _premiumFood = maxPremiumFood;
            Debug.Log("Przekroczono maksymalna ilość PremiumFood");
        }
    }
    public static int getPremiumFood()
    {
        return _premiumFood;
    }
    
    


}
