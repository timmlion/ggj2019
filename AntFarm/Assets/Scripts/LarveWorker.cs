﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LarveWorker : MonoBehaviour
{
    public float targetTime = 10.0f;
    public GameObject worker;

    void Update()
    {
        targetTime -= Time.deltaTime;

        if (targetTime <= 0.0f)
        {
            timerEnded();
        }
    }

    void timerEnded()
    {
        Instantiate(worker,new Vector3(9,-4.25f,-2), Quaternion.identity);
        Destroy(gameObject);

    }
}
