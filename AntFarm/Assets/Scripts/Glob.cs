﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Glob 
{
    public const int STATE_IDLE = 1;
    public const int STATE_GOINGFORFOOD = 2;
    public const int STATE_RETURNINGWITHFOOD = 3;
    public const int STATE_SEARCHINGFORFOOD = 4;

    public const int POSITION_SHAFT = 1;
    public const int POSITION_SLOPE = 2;
    public const int POSITION_LAND = 3;



    public static bool BARRACS = false;
    public static bool SLEEPINGQ = false;
    public static bool STOCKPILE = false;
}
