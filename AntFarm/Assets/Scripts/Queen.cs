﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Queen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponentInChildren<Canvas>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowCanvas(bool var)
    {
        gameObject.GetComponentInChildren<Canvas>().enabled = var;
        foreach (Button b in gameObject.GetComponentInChildren<Canvas>().GetComponentsInChildren<Button>())
        {
            switch (b.tag)
            {
                case "MakeAnt":
                    if(MainController.getBasicFood() >= 5)
                    {
                        b.interactable = true;
                    } else
                    {
                        b.interactable= false;
                    }
                    break;
                case "MakeWorrior":
                    if (Glob.BARRACS && MainController.getBasicFood() >= 10)
                    {
                        b.interactable = true;
                    }
                    else
                    {
                        b.interactable = false;
                    }
                    break;
            }
        }

    }

}
