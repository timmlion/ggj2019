﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntController : MonoBehaviour
{
    public static Shader outline;    
    public static Shader normal;
    public static GameObject slope;

    public static Collider leftSlopeEdge;
    public static Collider rightSlopeEdge;
    public static Collider topSlope;
    public static GameObject bottom;
    public static GameObject exit;
    //public static GameObject top;
    private void Start()
    {
        outline = Shader.Find("Outlined/Custom");
        normal = Shader.Find("Standard");
        slope = GameObject.FindGameObjectWithTag("heap");
        leftSlopeEdge = GameObject.FindGameObjectWithTag("LeftSlope").GetComponent<Collider>();
        rightSlopeEdge = GameObject.FindGameObjectWithTag("RightSlope").GetComponent<Collider>();
        topSlope = GameObject.FindGameObjectWithTag("TopSlope").GetComponent<Collider>();
        bottom = GameObject.FindGameObjectWithTag("Bottom");
        exit = GameObject.FindGameObjectWithTag("Exit");
        //top = GameObject.FindGameObjectWithTag("Top");
    }

    internal static void GoToTheTop(GameObject go)
    {
        go.GetComponent<Ant>().SetPosition(Glob.POSITION_SHAFT);
        MoveHorizontal(go, exit);
        
    }

    public static void SetOutline(GameObject go, bool haveOutline)
    {
        if (haveOutline) { 
            go.GetComponent<Renderer>().material.shader = outline;
        } else if (!haveOutline)
        {
            go.GetComponent<Renderer>().material.shader = normal;
        }
    }
    public static void SendForFood(GameObject gatherer, GameObject foodTarget)
    {
        Ant ant = gatherer.gameObject.GetComponent<Ant>();
        ant.SetState(Glob.STATE_GOINGFORFOOD);
        ant.SetFoodTarget(foodTarget);
        switch (ant.GetPosition())
        {
            case Glob.POSITION_SHAFT:
                //wydostań sie z shaftu
                break;
            case Glob.POSITION_SLOPE:
                
                break;
            case Glob.POSITION_LAND:
                MoveHorizontal(gatherer, foodTarget);
                break;
        }
    }

    internal static void GoDownTheShaft(GameObject go)
    {
        go.transform.rotation = Quaternion.Euler(90, 0, 180);
        go.gameObject.GetComponent<Ant>().StopAllCoroutines();
        go.gameObject.GetComponent<Ant>().StartCoroutine(
            go.GetComponent<Ant>().MoveToPosition(bottom, 4));

    }

    internal static void GoBackToWork(GameObject go)
    {
        Ant ant = go.gameObject.GetComponent<Ant>();
        go.transform.rotation = Quaternion.Euler(-90, 0, 180);
        ant.StopAllCoroutines();
        ant.StartCoroutine(
            ant.MoveUpSlope());
        ant.SetState(Glob.STATE_GOINGFORFOOD);
        ant.SetPosition(Glob.POSITION_SHAFT);
    }

    internal static void GoDownTheSlope(GameObject go, GameObject FoodTarget)
    {
        var distance1 = Vector3.Distance(rightSlopeEdge.transform.position, FoodTarget.transform.position);
        var distance2 = Vector3.Distance(leftSlopeEdge.transform.position, FoodTarget.transform.position);

        if (distance1 < distance2)
        {
            go.transform.Rotate(new Vector3(-45, 0, 0));
            go.gameObject.GetComponent<Ant>().StopAllCoroutines();
            go.gameObject.GetComponent<Ant>().StartCoroutine(
                go.GetComponent<Ant>().MoveToPosition(rightSlopeEdge.gameObject));
        } else
        {
            go.transform.Rotate(new Vector3(45, 0, 0));
            go.gameObject.GetComponent<Ant>().StopAllCoroutines();
            go.gameObject.GetComponent<Ant>().StartCoroutine(
                go.GetComponent<Ant>().MoveToPosition(leftSlopeEdge.gameObject));
        }


    }

    internal static void TurnTowordsFood(GameObject gameObject, GameObject FoodTarget)
    {
        gameObject.transform.LookAt(FoodTarget.transform);
    }

    public static void GoUpSlope(GameObject go)
    {
        go.transform.Rotate(new Vector3(-45, 0, 0));
        Debug.Log(topSlope.GetComponent<GameObject>());
        go.gameObject.GetComponent<Ant>().StopAllCoroutines();
        go.gameObject.GetComponent<Ant>().StartCoroutine(
            go.GetComponent<Ant>().MoveUpSlope());

    }

    public static void ReturnToStockPile(GameObject gatherer)
    {
        Ant ant = gatherer.gameObject.GetComponent<Ant>();
        switch (ant.GetPosition())
        {
            case Glob.POSITION_SHAFT:
                // go down the shaft
                break;
            case Glob.POSITION_SLOPE:
                GoUpSlope(gatherer);
                break;
            case Glob.POSITION_LAND:
                MoveToSlope(gatherer);
                break;
        }
    }

    private static void MoveToSlope(GameObject gatherer)
    {
        gatherer.transform.LookAt(new Vector3(slope.transform.position.x, 0));
        gatherer.gameObject.GetComponent<Ant>().StopAllCoroutines();
        gatherer.gameObject.GetComponent<Ant>().StartCoroutine(
            gatherer.gameObject.GetComponent<Ant>().MoveToPosition(DeterminSlopeSide(gatherer)));
        //gatherer.GetComponent<Ant>().SetPosition(Glob.POSITION_SLOPE);
    }

    private static GameObject DeterminSlopeSide(GameObject gatherer)
    {
        Vector3 gathererPos = gatherer.transform.position;
        var distLeft = Vector3.Distance(gathererPos, leftSlopeEdge.gameObject.transform.position);
        var distRight = Vector3.Distance(gathererPos, rightSlopeEdge.gameObject.transform.position);
        return distLeft < distRight?leftSlopeEdge.gameObject:rightSlopeEdge.gameObject;
    }

    private static void MoveHorizontal(GameObject gatherer, GameObject foodTarget)
    {
        // upewnij się że jesteś obrucpony w stronę jedzenie
        // idz w stronę jedzenia
        gatherer.transform.SetPositionAndRotation(gatherer.transform.position, Quaternion.Euler(0, gatherer.transform.rotation.eulerAngles.y, 0));
        gatherer.gameObject.GetComponent<Ant>().StartCoroutine(gatherer.gameObject.GetComponent<Ant>().MoveToPosition(foodTarget));
        
    } 
    private static void MoveVerticaly(GameObject gatherer, GameObject foodTarget)
    {

    }
}
