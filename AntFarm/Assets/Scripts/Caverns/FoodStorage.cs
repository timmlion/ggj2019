﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodStorage : MonoBehaviour
{
     
    private static int _maxStorage = 20;
    private int _currentStock = 0;

    public void SetCurrentStock(int ammount)
    {
        _currentStock += ammount;
        Debug.Log("Dodano: " + ammount + " jedzenia");
    }
    public int GetCurrentStock()
    {
        return _currentStock;
    }
    public static void SetMaxStorage(int storage)
    {
        _maxStorage = storage;
    }
    public static int GetMaxStorage()
    {
        return _maxStorage;
    }
}
