﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Produce_egg : MonoBehaviour
{
    public GameObject larvaWorker;
    public GameObject larvaWorrior;
    public GameObject position;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ProduceWorker()
    {
        Debug.Log("Produce Worker");
        if (GetComponent<Queen>() != null)
        {
            GetComponent<Queen>().ShowCanvas(false);
        }

        Instantiate(larvaWorker, position.transform.position, Quaternion.identity);
        MainController.SubtractFromBasicFood(5);
    }
    public void ProduceWorrior()
    {
        Debug.Log("Produce Worrior");
        if (GetComponent<Queen>() != null)
        {
            GetComponent<Queen>().ShowCanvas(false);
        }

        Instantiate(larvaWorrior, position.transform.position, Quaternion.identity);
        MainController.SubtractFromBasicFood(5);
    }
}
