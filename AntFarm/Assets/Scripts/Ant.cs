﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Ant : MonoBehaviour
{
    public int basicFood;
    public int premiumFood;
    public bool justBorn = true;

    [SerializeField]
    private int _state;
    private GameObject _foodTarget;
    private int _howMuchGather;
    [SerializeField]
    private int _position;



    private void Start()
    {
        SetState(Glob.STATE_GOINGFORFOOD); // DEBUG
        
        SetHowMuchGather(1); // DEBUG 
        //if (justBorn) AntController.GoToTheTop(gameObject);
    }


    // wykrycie kolizji z pokarmem do którego szła
    private void OnTriggerEnter(Collider other)
    {        
        GameObject go = other.gameObject;

        if (GetState() == Glob.STATE_GOINGFORFOOD 
            && GetPosition() == Glob.POSITION_LAND
            && other.gameObject == GetFoodTarget())
        {
            transform.LookAt(GetFoodTarget().transform);
            FoodControler.GatherFood(GetFoodTarget(), GetHowMuchGather(), this.gameObject);
            SetState(Glob.STATE_RETURNINGWITHFOOD);
            AntController.ReturnToStockPile(this.gameObject);
        }
        // dotknięto slopa
        else if (GetState() == Glob.STATE_RETURNINGWITHFOOD &&
            GetPosition() == Glob.POSITION_LAND &&
            (go.tag.Equals("RightSlope") || go.tag.Equals("LeftSlope")))
        {
            SetPosition(Glob.POSITION_SLOPE);
            AntController.GoUpSlope(this.gameObject);
        }
        // dotknięto top slopa
        else if (GetState() == Glob.STATE_RETURNINGWITHFOOD &&
                GetPosition() == Glob.POSITION_SLOPE &&
                go.tag.Equals("TopSlope"))
        {
            SetPosition(Glob.POSITION_SHAFT);
            AntController.GoDownTheShaft(this.gameObject);
        }
        // zrzut do magazynu
        else if (GetState() == Glob.STATE_RETURNINGWITHFOOD &&
            GetPosition() == Glob.POSITION_SHAFT &&
            go.tag.Equals("Structure") &&
            other.gameObject.GetComponent<FoodStorage>() != null &&
            other.gameObject.GetComponent<FoodStorage>().GetCurrentStock() < FoodStorage.GetMaxStorage())
        {            
            FoodControler.DeployFood(this.gameObject, other.gameObject);
            AntController.GoBackToWork(this.gameObject);
        }
        else if (GetState() == Glob.STATE_GOINGFORFOOD &&
            GetPosition() == Glob.POSITION_SHAFT &&
            go.tag.Equals("TopSlope") &&
            GetFoodTarget() != null)
        {
            AntController.TurnTowordsFood(this.gameObject, GetFoodTarget());
            AntController.GoDownTheSlope(this.gameObject, GetFoodTarget());
            SetPosition(Glob.POSITION_SLOPE);
        }
        else if (GetState() == Glob.STATE_GOINGFORFOOD &&
            GetPosition() == Glob.POSITION_SLOPE &&
            (go.tag.Equals("RightSlope") || go.tag.Equals("LeftSlope"))&&
            GetFoodTarget() != null)
        {
            SetPosition(Glob.POSITION_LAND);
            SetState(Glob.STATE_GOINGFORFOOD);
            AntController.SendForFood(this.gameObject, GetFoodTarget().gameObject);
            
        }
        
    }
    public IEnumerator MoveToPosition(GameObject foodTarget, int time = 2 )
    {
        var currentPos = transform.position;
        var t = 0f;
        
        while (t < 1)
        {
            t += Time.deltaTime / time;//timeToMove;
            transform.position = Vector3.Lerp(currentPos, foodTarget.transform.position, t);
            yield return null;
        }
    }
    public IEnumerator MoveUpSlope()
    {
        var currentPos = transform.position;
        var t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / 2;//timeToMove;
            transform.position = Vector3.Lerp(currentPos, new Vector3(-2.63f,9.6f, 1.32f), t);
            yield return null;
        }
    }
    public IEnumerator MoveDownSlope()
    {


        var currentPos = transform.position;
        var t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / 2;//timeToMove;
            transform.position = Vector3.Lerp(currentPos, new Vector3(-2.63f, 9.6f, 1.32f), t);
            yield return null;
        }
    }

    public void SetState(int state)
    {
        _state = state;
    }
    public int GetState()
    {
        return _state;
    }
    public void SetFoodTarget(GameObject target)
    {
        _foodTarget = target;
    }
    public GameObject GetFoodTarget()
    {
        return _foodTarget;
    }
    public void SetHowMuchGather(int ammount)
    {
        _howMuchGather = ammount;
    }
    public int GetHowMuchGather()
    {
        return _howMuchGather;
        ;
    }
    public void SetPosition(int pos)
    {
        _position = pos;
    }
    public int GetPosition()
    {
        return _position;
        ;
    }

}


