﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Mouse : MonoBehaviour
{
    public LayerMask Clicable;
    public AudioSource clickSound;
    // Update is called once per frame

    private GameObject clickedObject;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 
                Mathf.Infinity, Clicable))
            {
                clickSound.Play();
                if (hit.collider == null) return;
                switch (hit.collider.gameObject.tag)
                {
                    case "Ant":
                        Debug.Log("Jestem mrówka");
                        AntController.SetOutline(hit.collider.gameObject, true);
                        clickedObject = hit.collider.gameObject;
                        break;
                    case "Food":
                        if (clickedObject.tag == "Ant") 
                        {
                            // idz po ten resource
                            clickedObject.GetComponent<Ant>().SetState(Glob.STATE_GOINGFORFOOD);
                            AntController.SendForFood(clickedObject, hit.collider.gameObject);
                            
                            Debug.Log("Ide po resourca");
                        }
                        else
                        {
                            Debug.Log("Jestem Food");
                            clickedObject = hit.collider.gameObject;
                        }
                        break;
                    case "Structure":
                        Debug.Log("Jestem structure");
                        clickedObject = hit.collider.gameObject;
                        break;
                    case "Queen":
                        AntController.SetOutline(hit.collider.gameObject, true);
                        clickedObject = hit.collider.gameObject;
                        clickedObject.GetComponent<Queen>().ShowCanvas(true);
                        break;
                }
            }
            else
            {
                if (clickedObject != null)
                {                
                    AntController.SetOutline(clickedObject, false);
                    clickedObject = null;
                }
            }

        }
    }
}
